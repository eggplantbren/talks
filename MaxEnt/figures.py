import matplotlib.pyplot as plt
import numpy as np

# Fonts for plot
plt.rc("font", size=18, family="serif", serif="Computer Sans")
plt.rc("text", usetex=True)

figdir = "figures/"

# Make a discrete distribution
xs = np.array([1, 2, 3, 4, 5])
ps = np.array([1.0, 3.0, 2.0, 1.0, 0.3])
ps /= np.sum(ps)

plt.figure(figsize=(8, 6.5))
plt.bar(xs, ps, color=[0.2, 0.2, 0.2], width=0.5)
plt.gca().set_yticks([0, 0.1, 0.2, 0.3, 0.4, 0.5])
plt.xlabel("$x$", fontsize=22)
plt.ylabel("Probability")
plt.savefig(figdir + "probdist.pdf", transparent=True)

plt.gca().set_xticklabels(["", "A", "B", "C", "D", "E"])
plt.savefig(figdir + "probdist_letters.pdf", transparent=True)

