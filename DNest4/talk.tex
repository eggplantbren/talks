\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage{palatino}
\usepackage{subfig}
\usepackage{amsmath}
\usepackage{dsfont}
\usepackage{multimedia}

\usetheme{Warsaw}
\usecolortheme{crane}

\newcommand{\dnest}{{\tt DNest4}}

% www.sharelatex.com/learn/Beamer

\title{\dnest: Diffusive Nested Sampling in C++ and Python}
\author{Brendon J. Brewer}
\institute{Department of Statistics\\
The University of Auckland}
\date{{\tt \color{blue} https://www.stat.auckland.ac.nz/\~{ }brewer/}}

\begin{document}

\frame{\titlepage}





% -------------- New slide --------------
\begin{frame}
\frametitle{Rationale: Bayesian computation}
Posterior distribution:
\begin{align*}
p(\theta | D, M) &= \frac{p(\theta | M)p(D | \theta, M)}{p(D | M)}
\end{align*}

Marginal likelihood of model $M$, for model comparison later:
\begin{align*}
p(D | M) &= \int p(\theta | M)p(D | \theta, M) \, d\theta
\end{align*}

%Amount learned about $\theta$:
%\begin{align*}
%D_{\rm KL}\left[p(\theta | D, M)\, || \,p(\theta | M)\right]
%&= \int p(\theta | D, M)
%    \log\left[\frac{p(\theta | D, M)}{p(\theta | M)}\right] \, d\theta
%\end{align*}

How to compute these?

\end{frame}

% -------------- New slide --------------
\begin{frame}
\frametitle{``Computational'' notation}


\bgroup
\def\arraystretch{2}%  1 is the default, change whatever you need
\begin{tabular}{ll}
Prior               & $\pi(\theta)$\\
Likelihood          & $L(\theta)$\\
Marginal likelihood & $Z = \int \pi(\theta) L(\theta) \, d\theta$\\
%Information         & $H = \int \frac{\pi(\theta) L(\theta)}{Z}\log\left[\frac{\pi(\theta) L(\theta)/Z}{\pi(\theta)}\right] \, d\theta$
\end{tabular}
\egroup

\end{frame}


\begin{frame}
\frametitle{Standard tools}

\begin{itemize}
\item <2-> Stan
\item <3-> OpenBUGS, JAGS, etc.
\item <4-> Write your own Metropolis or whatever...
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Issues and popular answers}

Okay, but what about strong dependence between parameters?
\begin{center}
{\color{white} \bf Use Hamiltonian MCMC or similar!}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Issues and popular answers}

Okay, but what about strong dependence between parameters?
\begin{center}
{\color{red} \bf Use Hamiltonian MCMC or similar!}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Issues and popular answers}

Okay, but what about multiple peaks in the posterior?
\begin{center}
{\color{white} \bf Use annealing/tempering. Here's Radford Neal's website.
This'll also get you $Z$.}
\end{center}

\end{frame}


\begin{frame}
\frametitle{Issues and popular answers}

Okay, but what about multiple peaks in the posterior?
\begin{center}
{\color{red} \bf Use annealing/tempering. Here's Radford Neal's website.
This'll also get you $Z$.}
\end{center}

\end{frame}

\begin{frame}
\frametitle{Issues and popular answers}

Okay, but what about phase transitions?
\begin{center}
{\color{white} \bf Phase what?}
\end{center}

\end{frame}


\begin{frame}
\frametitle{Issues and popular answers}

Okay, but what about phase transitions?
\begin{center}
{\color{red} \bf Phase what?}
\end{center}

\end{frame}


\begin{frame}
\frametitle{Annealing}
Introduce distributions defined by
a temperature $T$ (by analogy with statistical mechanics):
\begin{align*}
p(\theta; T) &\propto \pi(\theta)L(\theta)^{1/T}
\end{align*}

\begin{tabular}{lcl}
$T = \infty$    &   $\implies$  & prior\\
$T = 1$           &   $\implies$  & posterior\\
$T = 0$         &   $\implies$  & delta function at maximum likelihood point
\end{tabular}

\vspace{10pt}
Typically, one ``cools'' from high $T$ down to $T=1$. No need to go below 1 ---
that's {\em overfitting}.

\end{frame}



\begin{frame}
\frametitle{Annealing}
Then use magical ``thermodynamic integral'' to get $Z$:

\begin{align}
\log(Z) &= \int_0^1 \mathds{E}\left[\log(L(\theta))\right]_{T=1/\beta} \, d\beta
\end{align}

\end{frame}



\begin{frame}
\frametitle{Phase transition}

BAD NEWS: This has a failure mode

\begin{center}
\movie{\includegraphics[width=0.5\textwidth, height=0.5\textwidth]
            {movie.png}}{movie.mkv}
\end{center}

\end{frame}


\begin{frame}
\frametitle{Nested Sampling uses {\em constrained priors}}
Instead of using a sequence of distributions
defined by $\pi(\theta)L(\theta)^{1/T}$, use a sequence defined by

\begin{align*}
p(\theta; \ell) &\propto \left\{
    \begin{array}{lr}
        \pi(\theta), & L(\theta) > \ell\\
        0,           & \textnormal{otherwise}.
    \end{array}
\right.
\end{align*}
and have $\ell$ increase at a controlled rate, so that enclosed prior mass
shrinks {\em geometrically}.

\end{frame}

\begin{frame}
\frametitle{Diffusive Nested Sampling}
Mixture of constrained priors

\includegraphics[width=0.9\linewidth]{figures/dns.pdf}



\end{frame}

\begin{frame}
\frametitle{Multimodality}

\begin{center}
\movie{\includegraphics[width=0.5\textwidth, height=0.5\textwidth]
            {movie.png}}{movie2.mp4}
\end{center}

\end{frame}



% -------------- New slide -------------
\begin{frame}
\frametitle{Astrostatistics example: exoplanets}

The radial velocity technique lets us find extrasolar planets...
\includegraphics[width=0.9\linewidth]{figures/exoplanet.pdf}

\end{frame}


% -------------- New slide -------------
\begin{frame}
\frametitle{A challenge for the radial velocity technique}
Can we distinguish these?\vspace{20pt}

Depends. In the limit (eccentricity $\to$ 0, stellar rotation signal $\to$ sinusoid), there is a genuine ambiguity. Away from this limit, it depends
on how informative the data is about the signal shape.\vspace{20pt}

{\bf Inference methods should tell us about the ambiguity}

\end{frame}


% -------------- New slide -------------
\begin{frame}
\frametitle{The Prior Information}
\begin{figure}
    \includegraphics[width=0.78\textwidth]{figures/RV_pgm_cropped.pdf}
\end{figure}


{\tiny
    Faria et al, 2016, A\&A 588, A31
    {\color{blue} arXiv: 1601.07495}
}

\end{frame}



% -------------- New slide -------------
\begin{frame}
\frametitle{HARPS Data}
\begin{figure}
    \includegraphics[width=0.7\textwidth]{figures/corot7_fit.pdf}
\end{figure}


{\tiny
    Faria et al, 2016, A\&A 588, A31
    {\color{blue} arXiv: 1601.07495}
}

\end{frame}



% -------------- New slide -------------
\begin{frame}
\frametitle{How many planets?}
\begin{figure}
    \includegraphics[width=0.8\textwidth]{figures/distN.pdf}
\end{figure}


{\tiny
    Faria et al, 2016, A\&A 588, A31
    {\color{blue} arXiv: 1601.07495}
}

\end{frame}





%%%%%%%%%%%%%%% New slide
\begin{frame}
\frametitle{Goodies!!!}

Software:\\
\hspace{0.1\textwidth}
{\tt \color{blue} https://github.com/eggplantbren/DNest4}\\

Paper:\\
\hspace{0.1\textwidth}
{\tt \color{blue} http://arxiv.org/abs/1606.03757}

\end{frame}

%%%%%%%%%%%%%%% New slide
\begin{frame}
\frametitle{The software package}
{\tt DNest4} is written in C++11. It is free software (MIT licenced).

You can implement models in three ways:
\begin{enumerate}
\item Write a C++ class
\item Write a Python class
\item Use the Python ``model builder''
\end{enumerate}

\end{frame}


%%%%%%%%%%%%%%% New slide
\begin{frame}
\frametitle{(1) Write a C++ class}

{\color{green} Advantages}: best performance, proposals can make use of
model structure for efficiency\vspace{1em}

{\color{red} Disadvantages}: have to know C++, can be quite a big job for
complex models.

\end{frame}

%%%%%%%%%%%%%%% New slide
\begin{frame}
\frametitle{(2) Write a Python class}

{\color{green} Advantages}: ``everyone'' knows Python\vspace{1em}

{\color{red} Disadvantages}: Slower performance, no multi-threading available

\end{frame}

\begin{frame}
\frametitle{(3) Use the Python model builder}

{\color{green} Advantages}: You write Python (which everyone knows), it
gets turned into C++ (which will be fast-ish). Feels Stan-ish.\vspace{1em}

{\color{red} Disadvantages}: Emitted C++ can be suboptimal and awkward to
edit. Not well documented, but see
{\tt \tiny https://plausibilitytheory.wordpress.com/2016/08/11/a-jags-like-interface-to-dnest4/}

\end{frame}



\begin{frame}
\frametitle{Acknowledgements}

Marsden fund \vspace{1em}

Collaborators: Dan Foreman-Mackey, Daniela Huppenkothen,
João Faria, Geraint Lewis, and others
\end{frame}

\end{document}


