import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rng

# Fonts for plot
plt.rc("font", size=18, family="serif", serif="Computer Sans")
plt.rc("text", usetex=True)

# Seed rng
rng.seed(3)

# Set up grid
x = np.linspace(-5, 5, 1001)
y = x.copy()[::-1]
[x, y] = np.meshgrid(x, y)

# Make gaussian
f = np.exp(-0.5*(x**2 + y**2))
f /= f.sum()

# Plot it
plt.figure(figsize=(8, 6.5))
plt.imshow(f, extent=[x.min(), x.max(), y.min(), y.max()], cmap="Blues")
plt.gca().set_xticks([-4, -2, 0, 2, 4])
plt.gca().set_yticks([-4, -2, 0, 2, 4])
plt.xlabel("$\\theta_1$")
plt.ylabel("$\\theta_2$")
plt.title("A 2D gaussian posterior distribution")
plt.savefig("gaussian1.pdf")

plt.plot(0.0, 0.0, "ro", label="Posterior mode")
plt.legend(loc="upper left")
plt.savefig("gaussian2.pdf")

xx, yy = rng.randn(20), rng.randn(20)
plt.plot(xx, yy, "g*", label="Posterior samples")
plt.legend(loc="upper left")
plt.savefig("gaussian3.pdf")

