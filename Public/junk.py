from pylab import *

rc("font", size=18, family="serif", serif="Computer Sans")
rc("text", usetex=True)

#sample = loadtxt("Bradmansample.txt")
#weights = loadtxt("Bradmanweights.txt")
#p = weights/weights.sum()
#w = weights/weights.max()

#ESS = int(np.exp(-np.sum(p*np.log(p + 1E-300))))
#posterior_sample = np.empty((ESS, sample.shape[1]))

#for i in range(0, ESS):
#    while True:
#        k = randint(sample.shape[0])
#        if rand() <= w[k]:
#            break
#    posterior_sample[i, :] = sample[k, :]

#hist(posterior_sample[:,0], 100)
#savetxt("posterior_sample_bradman.txt", posterior_sample)
#show()


x = loadtxt("posterior_sample_bradman.txt")
y = loadtxt("posterior_sample_taylor.txt")

tot = 0
for i in range(0, 10000):
    m, n = randint(x.shape[0]), randint(y.shape[0])
    if x[m, 0] >= y[n, 0]:
        tot += 1
print(tot/10000)

plt.figure(figsize=(9,6))
xx = arange(0, 51)
for i in range(0, 50):
    mu1, mu2, L = x[i, 0], x[i, 1], x[i, 2]
    mu = mu2 + (mu1 - mu2)*exp(-xx/L)
    if i==0:
        label = "Bradman"
    else:
        label = None
    plot(xx, mu, color="g", alpha=0.1, label=label)
xx = arange(0, 51)
for i in range(0, 50):
    mu1, mu2, L = y[i, 0], y[i, 1], y[i, 2]
    mu = mu2 + (mu1 - mu2)*exp(-xx/L)
    if i==0:
        label = "Taylor"
    else:
        label = None
    plot(xx, mu, color="k", alpha=0.1, label=label)

xlim([-0.5, 50.0])
ylim([-0.5, 150.0])
xlabel("Runs scored")
ylabel("Effective average (`ability')")
legend()
savefig("bradman-taylor.pdf", bbox_inches="tight")
show()

