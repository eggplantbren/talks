import numpy as np
import matplotlib.pyplot as plt

plt.rc("font", size=18, family="serif", serif="Computer Sans")
plt.rc("text", usetex=True)

x = np.arange(0, 51)
y = 45.0 - 20.0*np.exp(-x/5.0)

plt.figure(figsize=(9,6))
plt.plot(x, y, "o-", linewidth=2)
plt.xlim([-0.5, 50.0])
plt.ylim([-0.5, 50.0])
plt.xlabel("Runs scored")
plt.ylabel("Effective average (`ability')")
plt.savefig("eye-in.pdf", bbox_inches="tight")
plt.show()

