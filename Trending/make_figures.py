import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rng

HALF_LIFE = 400
rng.seed(123)

ts = np.linspace(0.0, 10.0, 1001)

lbc_change_times = 10.0*rng.rand(5)
lbc_change_amounts = np.exp(rng.randn(5))
lbc_change_amounts[4] *= -1.0
for i in range(len(lbc_change_times)):
    plt.plot(lbc_change_times[i], lbc_change_amounts[i], "go")
    plt.plot([lbc_change_times[i], lbc_change_times[i]],
             [0.0, lbc_change_amounts[i]], "g-")
plt.axhline(0.0, color="k")
plt.xlabel("Time (days)")
plt.ylabel("LBC changes")
plt.xlim([0.0, 10.0])
plt.show()

