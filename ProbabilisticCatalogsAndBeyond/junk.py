import numpy as np
import numpy.random as rng
import scipy.linalg as la
import matplotlib.pyplot as plt

# Set up Matplotlib stuff
plt.rc("font", size=20, family="serif", serif="Computer Sans")
plt.rc("text", usetex=True)
plt.rcParams["image.interpolation"] = "nearest"
plt.rcParams["image.cmap"] = "viridis"

# Set RNG seed
rng.seed(123)

# Load an orbit
orbit0 = np.loadtxt("orbits0.900.dat")
orbit = orbit0.copy()
for i in range(0, 9):
    orbit = np.vstack([orbit, orbit0])

# Plot keplerian signal
plt.figure(figsize=(12, 6))
plt.plot(np.linspace(0.0, 10.0, orbit.shape[0]), orbit[:,1],\
            "b-", linewidth=2, label="Keplerian exoplanet signal")
plt.xlabel("Time (periods)")
plt.ylabel("Radial Velocity (m/s)")
plt.hold(True)

# Now generate a stochastic signal
t = np.linspace(0.0, 10.0, 1001)
C = np.empty((len(t), len(t)))
for i in range(0, len(t)):
    for j in range(0, len(t)):
        dt = t[i] - t[j]
        C[i, j] = np.cos(2.0*np.pi*dt/1.0)*np.exp(-np.abs(dt)/50.0)
U = la.cholesky(C)
y = np.matrix(U.T)*np.matrix(rng.randn(len(t))).T

# Plot stochastic signal
plt.plot(t, y,\
        linewidth=2, linestyle="--", color="r", label="Stellar rotation signal")
plt.ylim([-3, 3])
plt.legend(loc="upper left")
plt.title("A challenge...")

# Save and display the figure
plt.savefig("figures/exoplanet.pdf", bbox_inches="tight")

