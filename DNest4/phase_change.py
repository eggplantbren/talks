"""
Make a movie demonstrating the problems with annealing.
"""
# Imports
import os
import numpy as np
import matplotlib.pyplot as plt
import dnest4.classic as dn4

# Fonts for plot
plt.rc("font", size=18, family="serif", serif="Computer Sans")
plt.rc("text", usetex=True)

# Print the warning about deleting files
print("WARNING! This will delete\
 movie.mkv and the Frames/ directory, if these exist.")
ch = input("Continue? y/n: ")
if ch != "y" and ch != "Y":
    exit()
os.system("rm -rf Frames/ movie.mkv")
os.mkdir("Frames")

# Hypothesis space and prior
x = np.linspace(-0.5, 0.5, 501)
y = np.linspace(0.5, -0.5, 501)
[x, y] = np.meshgrid(x, y)
p0 = np.ones(x.shape)
p0 /= p0.sum()

# Likelihood
def likelihood(x, y):
    rsq = x**2 + y**2
    L1 = np.exp(-0.5*rsq/0.01**2)/(2*np.pi*0.01**2)
    L2 = np.exp(-0.5*rsq/0.1**2)/(2*np.pi*0.1**2)
    return 1.0*L1 + 100.0*L2

def power_posterior(x, y, temperature=1.0):
    p = p0*likelihood(x, y)**(1.0/temperature)
    logZ = np.sum(p)
    p /= logZ
    return {"p": p, "logZ": logZ}

# Temperatures to loop over
temperatures = np.exp(np.linspace(5.0, 0.0, 301))

# Index
k = 1

# Save trajectory
H, logZ = [], []

for T in temperatures:
    result = power_posterior(x, y, T)

    plt.imshow(result["p"], interpolation="nearest", cmap="viridis",\
                vmin=0.0, vmax=result["p"].max(),\
                extent=[x.min(), x.max(), y.min(), y.max()])
    plt.title("Temperature={T}".format(T=np.round(T, 3)))
    if T < 25.0:
        ax = plt.gca()
        ax.text(-0.1, 0.1, "Good luck mixing!!!", fontsize=20, weight="bold", color="r")

    filename = "Frames/" + "%0.6d"%k + ".png"
    plt.savefig(filename)
    plt.close()
    print(filename)
    k += 1

# -vf scale=640:480 
os.system("ffmpeg -r 20 -i Frames/%06d.png -c:v h264 -b:v 4192k movie.mkv")

