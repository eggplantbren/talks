import numpy as np
import scipy.misc
import matplotlib.pyplot as plt

"""
Implements the drug example from "The Great Statistical Schism"
"""

# Use Dan FM's favourite fonts
plt.rc("font", size=20, family="serif", serif="Computer Sans")
plt.rc("text", usetex=True)

# Parameter space
theta = np.linspace(0.0, 1.0, 101)

# Prior
prior = np.ones(len(theta))*0.5/(len(theta) - 1)
prior[np.abs(theta - 0.7) <= 1E-7] = 0.5
assert np.abs(prior.sum() - 1.0) < 1E-7

# Calculate posterior
likelihood = theta**83*(1.0 - theta)**17
posterior = prior*likelihood
posterior /= posterior.sum()

# Plot as a barplot since it's a discrete distribution
width = 0.9*(theta[1] - theta[0])

# Plot the prior
plt.bar(theta - 0.5*width, prior, width=width, alpha=0.3, color="r", label="Prior")

# Plot the posterior
plt.bar(theta - 0.5*width, posterior, width=width, color="b", alpha=0.5,\
                        label="Posterior")
plt.legend(loc="upper left", fontsize=16)
plt.axis([0.0, 1.0, 0.0, 0.55])
plt.xlabel("Drug effectiveness $\\theta$")
plt.ylabel("Posterior Probability")
plt.savefig("figures/drug_posterior.pdf", bbox_inches="tight")

# Now do the p-values
x = np.arange(0, 101)

# p(x | H0)
p = scipy.misc.comb(100, x)*0.7**x*0.3**(1.0 - x)
p /= p.sum()

width = 0.9
plt.clf()
plt.bar(x - 0.5*width, p, width=width, color=[0.7, 0.7, 0.7],\
                        label="Sampling Distribution")
plt.xlabel("Number of recoveries $x$")
plt.ylabel("$p(x | \\theta=0.7)$")
plt.legend(loc="upper left")
plt.axis([-0.5, 100.5, 0.0, 0.15])
plt.savefig("figures/drug_sampling.pdf", bbox_inches="tight")

# Now the p-values
plt.bar(x[x >= 83] - 0.5*width, p[x >= 83], color="g")
plt.bar(x[x <= 47] - 0.5*width, p[x <= 47], color="y")
plt.axvline(83 - 0.5*width, linestyle="--", label="p-value summation limits")
plt.axvline(47 + 0.5*width, linestyle="--")

plt.legend(loc="upper left")
plt.savefig("figures/drug_pvalues.pdf", bbox_inches="tight")


